# enterprise-be

## 1.安装依赖
```
npm install
```

### 2.运行项目
```
npm run dev
```

### 3.文件描述
```
package.json安装依赖、执行脚本描述

src/index.js  源代码入口文件
src/db mongoose相关
src/routers  接口路由
src/test 随便写的js
src/utils 工具方法

关于状态码
200成功
0失败
```
