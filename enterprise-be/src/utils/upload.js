const fs = require('fs');

const saveFileToDisk = (ctx, filename) => {
  return new Promise((resolve, reject) => {
    // const file = ctx.request.body.files.file; 这样不对
    const file = ctx.request.files.file;
    const reader = fs.createReadStream(file.path);
    const writeStream = fs.createWriteStream(filename);

    reader.pipe(writeStream);

    reader.on('end', () => {
      resolve(filename);
    });

    reader.on('error', (err) => {
      reject(err);
    });
  });
};

//获取上传文件的后缀名
const getUploadFileExt = (ctx) => {
  const { name = '' } = ctx.request.files.file;

  return name.split('.').pop();
};
const getFileName = (ctx) => {
  const { name = 'name' } = ctx.request.files.file
  return name.split('.')[0]
}

module.exports = {
  saveFileToDisk,
  getUploadFileExt,
  getFileName
};
