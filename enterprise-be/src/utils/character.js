`
    -1 无任何权限   
    0 管理员权限
    1 增加权限
    2 删除权限
    3 查找权限
    4 修改权限
    
    只要有0 就默认别的权限都有
`;
//初始化数据库内容
const defaultCharacters = [
    {
        title: '管理员',
        name: 'admin',
        power: {
            product: [0],
            user: [0]
        }
    },
    {
        title: '普通成员',
        name: 'member',
        power: {
            product: [1],
            user: [-1]
        }
    }
]
module.exports={
    defaultCharacters
}