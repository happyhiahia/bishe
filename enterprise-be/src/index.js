const Koa = require('koa') //查找node_modules/koa/package.json中main字段 /lib/application.js
const koaBody = require('koa-body');
const { connect } = require('./db/index')
const registerRoutes = require('./routers/index')
const { middleware: koaJwtMiddleware, catchTokenError } = require('./utils/token')
const { logMiddleware } = require('./utils/log')
const cors = require('@koa/cors');

const app = new Koa()

connect().then(() => {
    app.use(cors());
    app.use(koaBody({
        multipart: true,
        formidable: {
            maxFileSize: 200*1024*1024  // 设置上传文件大小最大限制，默认2M
        }
    }));

    // app.use(catchTokenError)
    // koaJwtMiddleware(app)
    app.use(logMiddleware)


    registerRoutes(app)

    // // 2.绑定中间件
    // // 绑定第一层中间件  总耗时中间件
    // const respDurationMiddleware = require('./middleware/koa_response_duration')
    // app.use(respDurationMiddleware)
    // // 绑定第二层中间件  响应头中间件
    // //Content-Type:application/json charset=UTF-8
    // const respHeaderMiddleware = require('./middleware/koa_response_header')
    // app.use(respHeaderMiddleware)
    // // 绑定第三层中间件  业务逻辑中间件 读取文件内容
    // //访问http://localhost:3000/api/map接口返回数据
    // const respDataMiddleware = require('./middleware/koa_response_data')
    // app.use(respDataMiddleware)

    app.listen(3000, () => {
        console.log('服务启动成功');
    })

})


