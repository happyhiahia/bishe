const Router = require('@koa/router')
const mongoose = require('mongoose')
const File = mongoose.model('File');

const router = new Router({
    prefix: '/download'
})
const send = require('koa-send');
const sendfile = require('koa-sendfile');
//文件下载
router.get('/file/:name', async (ctx) => {
    console.log('调用下载方法');
    const name = ctx.params.name;

    const path = `upload/${name}`;
    console.log('name'.name, 'path', path);
  
    // ctx.attachment(path);
    // await send(ctx, path);
    ctx.attachment(decodeURI(path));
    // console.log('decodeURI(path)',decodeURI(path));
    await sendfile(ctx, decodeURI(path));
})
//download/list
router.get('/list', async (ctx) => {
    const list = await File
        .find()
        .sort({ _id: -1 })
        .exec()
    const total = await File.countDocuments()
    ctx.body = {
        msg: '获取文件列表成功',
        data: {
            list,
            total
        },
        code: 200
    }
})
module.exports = router