const Router = require('@koa/router')
const mongoose = require('mongoose')
const Auth = mongoose.model('Auth') //用户表
const Todo = mongoose.model('Todo') //待办
const Product = mongoose.model('Product');//产品
const PersonalDetail=mongoose.model('PersonalDetail') //把产品改为员工
const Customer = mongoose.model('Customer')//客户
const Project = mongoose.model('Project') //项目

const Nav = mongoose.model('Nav') //首页导航
const router = new Router({
    prefix: '/statistics',
})
/**首页统计 */
router.post('/getstatistics', async (ctx) => {
    let {
        userId
    } = ctx.request.body
    const authTotal = await Auth.countDocuments().exec()
    const todoTotal = await Todo.find({ userId: userId, state: 0 }).countDocuments().exec() ///
    // const productTotal = await Product.countDocuments().exec()
    const personalDetailTotal=await PersonalDetail.countDocuments().exec()
    const customerTotal = await Customer.countDocuments().exec()
    const projectTotal = await Project.countDocuments().exec()
    console.log('todoTotal', todoTotal);
    ctx.body = {
        data: {
            "authTotal": authTotal,
            "todoTotal": todoTotal,
            "personalDetailTotal": personalDetailTotal,
            "customerTotal": customerTotal,
            "projectTotal": projectTotal,
        },
        code: 200,
        msg: '获取全部列表成功'
    }
})
router.post('/addmynavlist', async (ctx) => {
    let {
        userId,
        navList
    } = ctx.request.body
    //userId是否存在
    const one = await Nav.findOne({
        userId: userId
    })
    if (!one) {
        //不存在 新增
        const nav = new Nav({
            userId,
            navList
        })
        const res = await nav.save()
        ctx.body = {
            data: res,
            msg: '新增成功',
            code: 200
        }
        return
    } else {
        //存在 更新
        one.navList = navList
        const res = await one.save() //更新成功
        ctx.body = {
            data: res,
            msg: '更新成功',
            code: 200
        }
    }
})
router.post('/getmynavlist', async (ctx) => {
    let {
        userId
    } = ctx.request.body
    const list = await Nav
        .find({ userId: userId }).exec()

    ctx.body = {
        data: {
            navList: list[0].navList
        },
        code: 200,
        msg: '获取列表成功'
    }
})
module.exports = router