const auth = require('./auth');
const inviteCode = require('./inviteCode')
const product = require('./product')
const user = require('./user')
const character = require('./character')
const log = require('./log')
const personaldetail = require('./personalDetail')
const upload = require('./upload')
const download = require('./download')
const todo = require('./todo')
const vacation = require('./vacation')
const customer = require('./customer')
const project = require('./project')
const order = require('./order')
const record = require('./record')
const statistics=require('./statistics')//首页接口 其他接口 
module.exports = (app) => {
    app.use(auth.routes())
    app.use(inviteCode.routes());
    app.use(product.routes());
    app.use(user.routes());
    app.use(character.routes())
    app.use(log.routes())
    app.use(personaldetail.routes())
    app.use(upload.routes())
    app.use(download.routes())
    app.use(todo.routes())
    app.use(vacation.routes())
    app.use(customer.routes())
    app.use(project.routes())
    app.use(order.routes())
    app.use(record.routes())
    app.use(statistics.routes())
}