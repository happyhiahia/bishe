const Router = require('@koa/router')
const mongoose = require('mongoose')

const Log = mongoose.model('Log')

const router = new Router({
    prefix: '/log'
})
router.get('/list', async (ctx) => {
    let {
        page,
        size
    } = ctx.query
    page = Number(page)
    size = Number(size)
    const list = await Log
        .find()
        .sort({
            _id:-1,
        })
        .skip((page - 1) * size)
        .limit(size)
        .exec()

    const total = await Log.countDocuments().exec()
    ctx.body = {
        data: {
            list,
            page,
            size,
            total
        },
        code: 200,
        msg: '获取日志列表成功'
    }
})

router.delete('/:id', async (ctx) => {
    const {
        id
    } = ctx.params
    const res = await Log.deleteOne({
        _id: id
    })
    ctx.body = {
        code: 200,
        msg: '删除成功',
        data: res
    }
})
module.exports = router