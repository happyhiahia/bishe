const Router = require('@koa/router')
const mongoose = require('mongoose')
const Record = mongoose.model('Record')

const router = new Router({
    prefix: '/record'
})
router.post('/add', async (ctx) => {
    let {
        userId,
        recordContent
    } = ctx.request.body

    const record = new Record({
        userId,
        recordContent
    })
    const res = await record.save()
    ctx.body = {
        data: res,
        code: 200,
        msg: '新增记录成功'
    }
})
router.post('/list', async (ctx) => {
    let {
        userId
    } = ctx.request.body
    const list = await Record
        .find({ userId: userId })
        .sort({
            _id: -1,
        })
        .exec()

    const total = await Record.countDocuments().exec()
    ctx.body = {
        data: {
            list,
            total
        },
        code: 200,
        msg: '获取列表成功'
    }
})
router.delete('/:id', async (ctx) => {
    const {
        id
    } = ctx.params
    const res = await Record.deleteOne({
        _id: id
    })
    ctx.body = {
        code: 200,
        msg: '删除成功',
        data: res
    }
})
module.exports = router