const mongoose = require('mongoose')
const { getMeta } = require('../helpers');
const ProjectSchema = new mongoose.Schema({
    //表_id即项目id
    projectName: String,//项目名
    pcusId:String,//客户id
    pcusName: String,//客户名  根据customer_id查找
    industry: String,//行业
    projectInfo: String,//项目简介
    price: String,//价格
    endDate: String,//项目截止日期
    projectState: String,//项目状态 0待开发 1进行中 2已完成
    meta: getMeta()
})
mongoose.model('Project', ProjectSchema)