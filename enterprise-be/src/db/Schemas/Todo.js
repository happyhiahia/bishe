const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const TodoSchema = new mongoose.Schema({
    userId:String,
    content: String,
    state: Number,//0  1 2待 在 已完成
    meta: getMeta(),
});

mongoose.model('Todo', TodoSchema);