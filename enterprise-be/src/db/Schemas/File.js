const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const FileSchema = new mongoose.Schema({
    filename: String,
    meta: getMeta(),
});

mongoose.model('File', FileSchema);