const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const NavSchema = new mongoose.Schema({
    //_id订单编号
    userId: String,//当前用户
    navList: Array,
    meta: getMeta(),
});

mongoose.model('Nav', NavSchema);
