const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const CharacterSchema = new mongoose.Schema({
    name:String,//member admin
    title:String,//成员 管理员
    power:Object,

    meta: getMeta(),
});

mongoose.model('Character',CharacterSchema);