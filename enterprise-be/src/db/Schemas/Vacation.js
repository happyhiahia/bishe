const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const VacationSchema = new mongoose.Schema({
    userId:String,//
    vacationPerson:String,//请假人
    startTime: String,
    endTime: String,
    reason: String,//请假原因
    state: Number,//0审批 1通过 2否决
    meta: getMeta(),
});

mongoose.model('Vacation',VacationSchema);