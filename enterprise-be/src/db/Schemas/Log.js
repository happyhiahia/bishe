const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const LogSchema = new mongoose.Schema({
    //用户
    user: {
        account: String,
        id: String,
    },
    //请求的方法、url
    request: {
        method: String,
        url: String,
        responseBody: String,
        status:Number
    },

    meta: getMeta(),
});

mongoose.model('Log', LogSchema);
