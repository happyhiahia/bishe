const mongoose = require('mongoose')
const { getMeta } = require('../helpers');
const ProductSchema = new mongoose.Schema({
    //产品名
    name: String,
    //价格
    price: Number,
    //著作人
    author: String,
    //上架日期
    publishDate: String,
    //类别
    classify: String,
    meta: getMeta()
})
mongoose.model('Product', ProductSchema)