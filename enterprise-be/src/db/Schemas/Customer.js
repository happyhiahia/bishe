const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const CustomerSchema = new mongoose.Schema({
    //表id即客户id
    cusName: String,//客户名
    legalPerson: String,//法人
    province: String,//省份
    cusType: String,//客户类型   0潜在客户 1有意向者 2真正客户 3历史客户
    satisfaction: Number,//满意度 1-5星
    credit: Number,//信用度  1-5星
    cusAddress: String,//地址
    cusPhone: String,//电话
    cusWebsite: String,//网站
    cusFund: String,//注册资金
    cusLicence: String,//注册号

    meta: getMeta(),
});

mongoose.model('Customer', CustomerSchema);