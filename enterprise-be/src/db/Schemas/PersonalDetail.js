const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const PersonalDetailSchema = new mongoose.Schema({
    account:String, //用户账号 anth表的username字段
    realName:String,//用户姓名
    sex:Number,//性别 0男 1女
    birthday:String, //生日
    phoneNumber:String,//手机号码
    home:String,//籍贯
    marriage:Number,//婚姻状况  0未婚 1已婚 2离异
    //待加 部门字段
    meta: getMeta(),
});

mongoose.model('PersonalDetail', PersonalDetailSchema);