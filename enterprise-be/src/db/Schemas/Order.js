const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const OrderSchema = new mongoose.Schema({
    //_id订单编号
    cusId: String,//客户id
    cusName: String,//客户名称
    cusPhone: String,//联系电话
    projectId: String,//项目id
    projectName: String,//项目名称
    orderDate: String,//下单日期
    payState: String,//0待支付 1已支付
    meta: getMeta(),
});

mongoose.model('Order', OrderSchema);
