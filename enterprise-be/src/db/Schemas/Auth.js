const mongoose = require('mongoose');
const { getMeta } = require('../helpers');

const AuthSchema = new mongoose.Schema({
    username: String,
    password: String,
    character:String,
    meta: getMeta(),
});

mongoose.model('Auth',AuthSchema);