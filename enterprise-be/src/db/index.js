require('./Schemas/Auth');
require('./Schemas/inviteCode')
require('./Schemas/Product')
require('./Schemas/Character')
require('./Schemas/Log')
require('./Schemas/PersonalDetail')
require('./Schemas/File')
require('./Schemas/Todo')
require('./Schemas/Vacation')
require('./Schemas/Customer') //客户基本信息
require('./Schemas/Project') //项目信息表
require('./Schemas/Order') //订单表
require('./Schemas/Record') //交往记录表
require('./Schemas/Nav') //首页快捷导航
const mongoose = require('mongoose')

const connect = () => {
    return new Promise((resolve) => {
        // 去连接数据库
        mongoose.connect('mongodb://127.0.0.1:27017/enterprise');

        // 当数据库被打开的时候 做一些事情
        mongoose.connection.on('open', () => {
            console.log('连接数据库成功');

            resolve();
        });
    });
};

module.exports = {
    connect,
};





// const AuthSchema = new mongoose.Schema({
//     username: String,
//     password: String
// })
// const AuthModal = mongoose.model('Auth', AuthSchema)

// const connect = () => {
//     //连接本地enterprise数据库 没有会自动新建
//     mongoose.connect('mongodb://127.0.0.1:27017/enterprise')

//     //当数据库被打开时
//     mongoose.connection.on('open', () => {
//         console.log('连接成功');

//         //创建文档
//         const auth = new AuthModal({
//             username: '李华',
//             password: 'lihua'
//         })
//         //保存，同步到mongodb
//         auth.save()
//     })
// }
