const mongoose = require('mongoose')
const { connect } = require('../db/index')
const character = require('../utils/character')
const { defaultCharacters } = character

const Character = mongoose.model('Character')
//连接数据库后初始化character表
connect()
    .then(async () => {
        console.log('开始初始化角色集合');
        await Character.insertMany(defaultCharacters)
        console.log('角色集合初始化完成');
    })
console.log(defaultCharacters);
