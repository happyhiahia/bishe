import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store'
import Antd from 'ant-design-vue';
import { regDirectives } from './utils/directive';

//echart图样式
import './assets/styles/reset.css';
// 引入全局的样式文件
import './assets/styles/global.css';

import axios from 'axios'
// 大屏echart数据请求基准路径的配置
// axios.defaults.baseURL = 'http://127.0.0.1:3000/api/'

const app = createApp(App);
regDirectives(app)
app.use(store).use(router).use(Antd).mount('#app')
