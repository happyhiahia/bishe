import { createRouter, createWebHashHistory } from 'vue-router'

// import { character } from '../service'
import store from '../store/index'
//router匹配上到下？？ /auth访问到/
const routes = [
  {
    path: '/auth',
    name: 'Auth',
    component: () => import(/* webpackChunkName: "Auth" */ '../views/Auth'),
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home'),
  },

  {
    path: '/',
    name: 'Layout',
    redirect: '/auth',
    component: () => import(/* webpackChunkName: "Auth" */ '../views/layout/Layout'),
    children: [
      {
        // 产品管理 改用户管理
        path: '/products',
        name: 'Products',
        component: () => import(/* webpackChunkName: "products" */ '../views/products'),
      },

      /**系统管理 */
      {
        // 角色管理
        path: '/users',
        name: 'Users',
        component: () => import(/* webpackChunkName: "systems" */ '../views/users'),
      },
      {
        // 日志管理
        path: '/logs',
        name: 'Logs',
        component: () => import(/* webpackChunkName: "systems" */ '../views/logs'),
      },
      {
        // 员工管理
        path: '/staffs',
        name: 'Staffs',
        component: () => import(/* webpackChunkName: "systems" */ '../views/system/StaffManage'),
      },

      /*员工管理*/
      {
        path: '/todolist',// 人员管理 去掉，改成代办
        name: 'TodoList',
        component: () => import(/* webpackChunkName: "Users" */ '../views/staff/TodoList.vue'),
      },
      {
        path: '/vacationmanage', // 请假管理 admin
        name: 'VacationManage',
        component: () => import(/* webpackChunkName: "Users" */ '../views/staff/VacationManage.vue'),
      },
      {
        path: '/vacationprocess', // 请假流程
        name: 'VacationProcess',
        component: () => import(/* webpackChunkName: "Users" */ '../views/staff/VacationProcess.vue'),
      },
      {
        path: '/personaldetail', // 员工个人信息
        name: 'PersonalDetail',
        component: () => import(/* webpackChunkName: "Users" */ '../views/staff/PersonalDetail.vue'),
      },
      {
        path: '/companyannounces',// 公司公告
        name: 'CompanyAnnounces',
        component: () => import(/* webpackChunkName: "Users" */ '../views/staff/CompanyAnnounces.vue'),
      },
      {
        path: '/home',
        name: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home'),
      },

      /**客户管理 */
      {
        path: '/clientlife',
        name: 'ClientLife',
        component: () => import(/* webpackChunkName: "client" */ '../views/client/ClientLife'),
      },
      {
        path: '/clientarea',
        name: 'ClientArea',
        component: () => import(/* webpackChunkName: "client" */ '../views/client/ClientArea'),
      },
      {
        path: '/clientinfo',
        name: 'ClientInfo',
        component: () => import(/* webpackChunkName: "client" */ '../views/client/ClientInfo'),
      },
      {
        path: '/orderinfo',
        name: 'OrderInfo',
        component: () => import(/* webpackChunkName: "client" */ '../views/client/OrderInfo'),
      },
      {
        path: '/businessrecord',
        name: 'BusinessRecord',
        component: () => import(/* webpackChunkName: "client" */ '../views/client/BusinessRecord'),
      },
      /**数据分析 */
      {
        path: 'client',
        name: 'Client',
        component: () => import(/* webpackChunkName: "analysis" */ '../views/analysis/Client'),//客户构成分析
      },
      {
        path: 'companybusiness',
        name: 'CompanyBusiness',
        component: () => import(/* webpackChunkName: "analysis" */ '../views/analysis/CompanyBusiness'),//公司业务数据分析
      }
    ]
  },

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})
router.beforeEach(async (to, from, next) => {
  // const reqArr = []
  if (!store.state.characterInfo.length) {
    // reqArr.push(store.dispatch('getCharacterInfo'))
    await store.dispatch('getCharacterInfo')   //获取角色信息
  }
  if (!store.state.userInfo.username) {
    // reqArr.push(store.dispatch('getUserInfo'))
    await store.dispatch('getUserInfo') //设置token
  }

  // await Promise.all(reqArr)
  next()
})

export default router
