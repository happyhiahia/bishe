export default [
    /** 
     * 只有管理员可见
     *一级： 系统管理：角色管理、日志管理、人员管理
     *二级： 
    */
    {
        title: '首页',
        url: '/home',
        onlyAdmin: false,
    },
    
    {
        title: '员工管理',
        onlyAdmin: false,
        children: [
            //PersonalDetail 添加按钮后期只有管理员可
            //获取列表接口 admin   查询单条member
            {
                title: '我的代办',
                url: '/todolist',
                onlyAdmin: false,
            },

            {
                title: '请假流程',
                url: '/vacationprocess',
                onlyAdmin: false,
            },
            {
                title: '公司公告',
                url: '/companyannounces',
                onlyAdmin: false,
            },
            {
                title: '员工个人消息',
                url: '/personaldetail',
                onlyAdmin: false,
            },
        ],
    },
    {
        title: '请假管理',
        url: '/vacationmanage',
        onlyAdmin: true,
    },
    {
        title: '客户管理',
        onlyAdmin: false,
        children: [
            {
                title: '客户基本信息',
                url: '/clientinfo',
                onlyAdmin: false,
            },
            {
                title: '项目信息',
                url: '/clientlife',
                onlyAdmin: false,
            },
            {
                title: '订单信息',
                url: '/orderinfo',
                onlyAdmin: false,
            },

            {
                title: '来往记录',
                url: '/businessrecord',
                onlyAdmin: false,
            },
            {
                title: '客户地域分布',
                url: '/clientarea',
                onlyAdmin: false,
            },

        ],
    },
    {
        title: '数据分析',
        onlyAdmin: false,
        children: [
            {
                title: '客户构成分析',
                url: '/client',
                onlyAdmin: false,
            },
            {
                title: '公司业务数据分析',
                url: '/companybusiness',
                onlyAdmin: false,
            }
        ],
    },
    {
        title: '系统管理',
        onlyAdmin: true,
        children: [
            // {
            //     title: '字典管理',
            //     url: '/invite-code',
            //     onlyAdmin: true,
            // },

            // {
            //     title: '用户管理',
            //     url: '/invite-code',
            //     onlyAdmin: true,
            // },
            ////
            // {
            //     title: '旧员工管理', //产品管理改的
            //     url: '/products',
            //     onlyAdmin: false,
            // },
            {
                title: '员工管理',
                url: '/staffs',
                onlyAdmin: true,
            },
            {
                title: '角色管理',
                url: '/users',
                onlyAdmin: true,
            },
            //日志管理，只有管理员可见
            {
                title: '日志管理',
                url: '/logs',
                onlyAdmin: true,
            },

        ],
    },
];