//日志列表，将路径和功能对应
const LOG_MAP = [
    ['/character/list', '获取角色列表'],
    ['/log/list', '获取日志列表'] ,//显示的时候带有参数
    ['/user/info','获取自己的登入信息'],
];
export const getLogInfoByPath = (path) => {
    let title = ''
    LOG_MAP.forEach((item) => {
        if (path.includes(item[0])) {
            // title=item[1]
            title = path.replace(item[0], item[1])
        }
    })
    return title || path
}