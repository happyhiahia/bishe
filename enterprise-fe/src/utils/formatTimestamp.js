//格式化时间戳为时间格式
export const formatTimestamp = (ts) => {
    const date = new Date(Number(ts))

    const YYYY = date.getFullYear()
    const MM = date.getMonth() + 1
    const DD = date.getDate()
    // const hh=date.getHours()
    // const mm=date.getMinutes()
    // const ss=date.getSeconds()

    // return `${YYYY}/${MM}/${DD} ${hh}:${mm}:${ss}`
    return `${YYYY}/${MM}/${DD}`
}