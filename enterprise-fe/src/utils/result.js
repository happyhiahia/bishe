import { message } from 'ant-design-vue';

export const result = (response, authShowErrorMsg = true) => {
    const { data } = response;

    // if ((data.code === 0) && authShowErrorMsg) {
    //     message.error(data.msg);
    // }

    return {
        success(callback) {
            if (data.code == 200) {
                callback(data, response);
                message.success(data.msg);
            }
            return this;
        },
        fail(callback) {
            if (data.code === 0) {
                callback(data, response);
                message.error(data.msg);
            }
            return this;
        },
        finally(callback) {
            callback(data, response);
            return this;
        },
    };
};
export const deepClone=(obj)=>{
    return JSON.parse(JSON.stringify(obj))
}