/**
 * 将标准时间进行处理
 * Sun Jan 17 2021 00:00:00 GMT+0800 (中国标准时间)
 * 2020-01-01 12:00:00
 */

//p为不够10添加0的函数
const p = (s) => {
    return s < 10 ? "0" + s : s;
};
export function timeTransfer(oldTime) {
    const d = new Date(oldTime);
    //2020-01-01 
    const resDate =
        d.getFullYear() + "-" + p(d.getMonth() + 1) + "-" + p(d.getDate());
    return resDate
}

export function timeTransferToTime(d) {
    //2020-01-01 
    const resDate =
        d.getHours() + ":" + p(d.getMinutes() ) + ":" + p(d.getSeconds());
    return resDate
}