import axios from 'axios'

export const add = ({ ...form }) => {
    return axios.post('http://localhost:3000/record/add', form)
}
export const list = (userId) => {
    return axios.post('http://localhost:3000/record/list', {
        userId
    })
}
export const remove = (id) => {
    return axios.delete(
        `http://localhost:3000/record/${id}`
    )
}