import axios from 'axios';
import { character } from '.';
//获取用户列表
export const list = (page = 1, size = 10, keyword = "") => {
    return axios.get('http://localhost:3000/user/list', {
        params: {
            page,
            size,
            keyword
        }
    })
}
//删除用户
export const remove = (id) => {
    return axios.delete(`http://localhost:3000/user/${id}`)
}
//添加用户
export const add = (username, password, character) => {
    return axios.post('http://localhost:3000/user/add', {
        username,
        password,
        character
    })
}
//重置密码
export const resetPassword = (id) => {
    return axios.post('http://localhost:3000/user/reset/password', {
        id
    })
}
//修改角色
export const editCharacter = (characterId, userId) => {
    return axios.post('http://localhost:3000/user/updatecharacter', {
        character: characterId,
        userId: userId
    })
}
export const info = () => {
    return axios.get('http://localhost:3000/user/info')
}

//批量上传用户
export const addMany = (key) => {
    return axios.post('http://localhost:3000/user/addMany', {
        key
    })
}