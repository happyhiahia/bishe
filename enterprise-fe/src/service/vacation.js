import axios from 'axios'
export const add = ({ ...form }) => {
    return axios.post('http://localhost:3000/vacation/add', form)
}
export const list = (data) => {
    return axios.get('http://localhost:3000/vacation/list', {
        params: data
    })
}
export const getVacationState = (userId) => {
    return axios.post('http://localhost:3000/vacation/getVacationState', {
        userId
    })
}

/**管理端*/
export const changeState = (id, state) => {
    return axios.post('http://localhost:3000/vacation/changeState',
        {
            id,
            state
        }
    )
}

//删除用户
export const removeVacation = (id) => {
    return axios.delete(`http://localhost:3000/vacation/${id}`)
}