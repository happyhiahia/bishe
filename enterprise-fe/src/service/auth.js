import axios from 'axios'

export const register = (username, password,inviteCode) => {
   return axios.post('http://localhost:3000/auth/register', {
        username,
        password,
        inviteCode
    })
};

export const login = (username, password) => {
    return axios.post('http://localhost:3000/auth/login', {
        username,
        password
    })
};
