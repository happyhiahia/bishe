import axios from 'axios'

export const add = (userId, content, state) => {
    return axios.post('http://localhost:3000/todo/add', {
        userId,
        content,
        state,
    })
};
export const list = (userId, type) => {
    return axios.post('http://localhost:3000/todo/list', {
        userId,
        type
    })
}
//删除
export const remove = (id) => {
    return axios.delete(
        `http://localhost:3000/todo/${id}`
    )
}
//更改
export const update = (id, state) => {
    return axios.post('http://localhost:3000/todo/update', {
        id,
        state
    })
}



