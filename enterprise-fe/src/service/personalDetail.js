import axios from 'axios';

export const add = ({ ...form }) => {
    return axios.post('http://localhost:3000/personaldetail/add', form)
}

export const search = (account) => {
    return axios.post('http://localhost:3000/personaldetail/search',
        {
            account
        }
    )
}
//员工管理列表
export const list = (page = 1, size =5, keyword = "") => {
    return axios.get('http://localhost:3000/personaldetail/list', {
        params: {
            page,
            size,
            keyword
        }
    })
}
//员工管理-删除
export const remove = (id) => {
    return axios.delete(
        `http://localhost:3000/personaldetail/${id}`
    )
}

