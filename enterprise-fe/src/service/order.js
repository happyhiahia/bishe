import axios from 'axios'
export const add = ({ ...form }) => {
    return axios.post('http://localhost:3000/order/updateandadd', form)
}
export const list = (page = 1, size = 10, keyword = "") => {
    return axios.get('http://localhost:3000/order/list', {
        params: {
            page,
            size,
            keyword
        }
    })
}
export const remove = (id) => {
    return axios.delete(`http://localhost:3000/order/${id}`)
}


