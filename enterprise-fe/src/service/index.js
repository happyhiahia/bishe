import axios from 'axios';
import { getToken } from '../utils/token';
export * as home from './home'
export * as auth from './auth'; //把./auth/index.js写的函数统一做为auth对象导出出去
export * as product from './product';
export * as user from './user';
export * as character from './character'
export * as log from './log'
export * as file from './file'
export * as todo from './todo'
export * as vacation from './vacation'
/**客户管理模块 */
export * as customer from './customer'
export * as project from './project'
export * as order from './order'
export * as record from './record' 
/**数据分析模块 */
export * as analysis from './analysis'
/**员工管理 */
export * as personalDetail from './personalDetail' //员工个人消息
axios.defaults.headers['Authorization'] = `Bearer ${getToken()}` //每次请求携带token  