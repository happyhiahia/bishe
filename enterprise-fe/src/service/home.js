import axios from 'axios'
export const list = (userId) => {
    return axios.post('http://localhost:3000/statistics/getstatistics', {
        userId
    })
}

/**添加导航 */
export const addnav = (userId, navList) => {
    return axios.post('http://localhost:3000/statistics/addmynavlist', {
        userId,
        navList
    })
}
/**获取当前id用户的导航表 */
export const getnav = (userId) => {
    return axios.post('http://localhost:3000/statistics/getmynavlist', {
        userId
    })
}