import axios from 'axios';
import { getToken } from '../utils/token';

// axios.defaults.headers['Authorization']=`Bearer ${getToken()}` //每次请求携带token  
export const add = ({ ...form }) => {
    return axios.post('http://localhost:3000/product/add', form)
}
export const list = (data) => {
    return axios.get('http://localhost:3000/product/list', {
        params: data,
        // headers:{
        //     Authorization:`Bearer ${getToken()}`
        // } //这样每个写麻烦，同一配置
    })
}

export const remove = (id) => {
    return axios.delete(
        `http://localhost:3000/product/${id}`
    )
}