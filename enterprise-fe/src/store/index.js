import { createStore, Store } from 'vuex'
import { character, customer, project, user } from '../service'
import { result } from '../utils/result'
import { getCharacterInfoById } from '../utils/character'

export default createStore({
  state: {
    characterInfo: [],
    userInfo: {},
    userCharacter: {},
    customerInfo: [],
    projectInfo: []
  },
  //函数集合 修改state
  mutations: {
    setCharacterInfo(state, characterInfo) {
      state.characterInfo = characterInfo
    },
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    setUserCharacter(state, userCharacter) {
      state.userCharacter = userCharacter
    },
    setCustomerInfo(state, param) {
      state.customerInfo = param
    },
    setProjectInfo(state, param) {
      state.projectInfo = param
    }
  },
  actions: {
    async getCharacterInfo(store) {
      const res = await character.list()
      result(res)
        .success(({ data }) => {
          store.commit('setCharacterInfo', data)
        })
    },
    async getUserInfo(store) {
      const res = await user.info()
      result(res)
        .success(({ data }) => {
          store.commit('setUserInfo', data)
          store.commit('setUserCharacter', getCharacterInfoById(data.character))
          // console.log('store.state', store.state);
        })
    },
    async getCustomerInfo(store) {
      const res = await customer.list()
      if (res.data.code == 200) {
        store.commit('setCustomerInfo', res.data.data)
      }
    },
    async getProjectInfo(store) {
      const res = await project.list()
      if (res.data.code == 200) {
        store.commit('setProjectInfo', res.data.data)
      }
    }

  },
  modules: {

  }
  //直接设置数据放到mutation中，如果之前有前置操作actions
})
