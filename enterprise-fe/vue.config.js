module.exports = {
    css: {
        loaderOptions: {
            less: {
                javascriptEnabled: true,//允许链式调用的换行
            }
        }
    },

    devServer: {
        port: 8899, //  端口号的配置
        open: true // 自动打开浏览器
    }

}
